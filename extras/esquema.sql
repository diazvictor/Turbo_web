PRAGMA foreign_keys = on;
drop table if exists parrilla;
create table if not exists parrilla( --Tabla parrilla sqlite3
    id_parrilla    integer primary key AUTOINCREMENT,
    desde          datetime default (datetime('now','localtime')), --la fecha desde
    hasta          datetime default (datetime('now','localtime')), --la fecha hasta
    nombre         varchar(64) not null, -- nombre del video
    descripcion    varchar(128) not null,  --descripcion del video
    etiquetas      varchar(128) default '', --etiquetas del video(separadas por coma)
    nombre_archivo varchar(128) default '', --etiquetas del video(separadas por coma)
    estatus        varchar(1) not null check (estatus in ('f','t')) default 't' 
);
create unique  index parrilla_id_parrilla on parrilla (id_parrilla);
create  index parrilla_nombre on parrilla (nombre);
create  index parrilla_descripcion on parrilla (descripcion);
create  index parrilla_etiquetas on parrilla (etiquetas);

INSERT INTO parrilla VALUES(1,'2016-04-10 19:00:25','2017-04-10 19:00:25','akira','akira es una peli muy buena y con mucho gore','gore,accion,ficcion','','t');

INSERT INTO parrilla VALUES(2,'2016-04-10 19:00:25','2017-04-10 19:00:25','ajin','ajin es una serie muy buena y con mucho horror','horro,accion,ficcion','','t');



select * from parrilla;
