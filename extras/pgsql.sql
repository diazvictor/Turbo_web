/*
         _                 ____ _     _ 
        | |   _   _  __ _ / ___| |__ (_)
        | |  | | | |/ _` | |   | '_ \| |
        | |__| |_| | (_| | |___| | | | |
        |_____\__,_|\__,_|\____|_| |_|_|

  Copyright (c) 2016 Díaz Víctor aka (Máster Vitronic)
    <vitronic2@gmail.com> <vitronic@vitronic.com.ve>
*/

drop table if exists demo cascade;
create table if not exists demo(
    id_demo         serial not null primary key,
    nombre          varchar(128) not null,
    edad            smallint not null,
    genero          char(1) not null default 'm',
    algun_mensaje   varchar(256) default ''
);
create unique index demo_nombre on demo (nombre);
alter table public.demo OWNER to dabla_user;
