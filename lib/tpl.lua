
local tpl = class("tpl")

function tpl:get(tpl)
    local tpl = string.format("%s/%s/%s/%s", conf.path , conf.tpl , conf.tema, tpl)
    local f = io.open(tpl, "rb")
    local r = f:read("*all")
    f:close()
    return r
end

return tpl
