--[[
         _                 ____ _     _
        | |   _   _  __ _ / ___| |__ (_)
        | |  | | | |/ _` | |   | '_ \| |
        | |__| |_| | (_| | |___| | | | |
        |_____\__,_|\__,_|\____|_| |_|_|

  Copyright (c) 2016 Díaz Víctor aka (Máster Vitronic)
    <vitronic2@gmail.com> <vitronic@vitronic.com.ve>
]]--

--[[HAy que aplicar el patron singleton aqui]]--


lsql = {} -- Declaro esto como una tabla (Array)
lsql.__index = lsql -- Este sera el indice de esta clase

--conexion a la base de datos
function lsql.connect()

    if luachi.db.tipo_db == 'pgsql' then
        --[[ Cargamos el modulo postgres ]]--
        local driver = require "luasql.postgres"
        env = assert (driver.postgres())
        con_db = assert (env:connect(luachi.db.db,luachi.db.user,luachi.db.passwd,luachi.db.host))
    elseif luachi.db.tipo_db == 'sqlite3' then
        --[[ Cargamos el modulo sqlite3 ]]--
        local driver = require "luasql.sqlite3"
        env = assert(driver.sqlite3())
        con_db = assert(env:connect(luachi.db.db))
    end
   return con_db

end

--cierra la conexion
function lsql.close()
    env:close()
    con_db:close()
end

-- Reads the cursor information after reading from db and returns a table
-- @autor https://github.com/sailorproject/sailor
local function fetch_row(cur, res)
	res = res or {}
	local row = cur:fetch ({}, "a")
	if not row then
		cur:close()
		return res
	end
	local types = cur:getcoltypes()
	local names = cur:getcolnames()
	for k,t in pairs(types) do
		if t:find('number') or t:find('int') then
                    row[names[k]] = tonumber(row[names[k]])
		end
	end
	table.insert(res,row)
	return fetch_row(cur,res)
end

-- Runs a query
-- @param query string: the query to be executed
-- @return table: the rows with the results
function lsql.get_results(query)
	local cur = assert(lsql.connect():execute(query))
	if type(cur) == 'number' then
		return cur
	end
	return fetch_row(cur)
end

-- ejecuta una consulta sql
-- @param query string: el query a ser ejecutado
-- @return string: el error | number: el resultado
function lsql.query(sql)
    local cur,errorString = lsql.connect():execute(sql)
    if type(cur) == 'number' then
        return cur
    else
        return errorString
    end
end

-- Ejecuta una consulta y retorna el primer valor
-- encontrado si no se le pasa la clausula where
-- @param query string: la consulta a ejecutar
-- @return string | number: the result
function lsql.get_var(query)
    local res = lsql.get_results(query)
    local value
    if next(res) then
        for _,v in pairs(res[1]) do
            value = v
        end
    end
    return value
end

-- Ejecuta una consulta y retorna todas la filas
-- @param query string: la consulta a ejecutar
-- @return array|table the result
function lsql.get_rows(sql)
    local cursor = assert(lsql.connect():execute(sql))
    return function ()
        return cursor:fetch()
    end
end

-- Ejecuta una consulta y retorna toda la primera fila
-- encontrado si no se le pasa la clausula where
-- @param query string: la consulta a ejecutar
-- @return array|table the result
function lsql.get_row(sql)
    local cur = assert (lsql.connect():execute(sql))
    return cur:fetch ({}, "a") --,lsql.close()
end


-- retorna una version limpia y desinfectada de la entrada (version para la base de datos)
-- @param string
-- @return string: desinfectados
function lsql.escape(s)
    return lsql.connect():escape(s)
end

-- retorna una version limpia y desinfectada de la entrada
-- @param string | array
-- @return string | array desinfectados
function lsql.sanitize(q)
    if type(q) == "string" then
        q = trim(lsql.escape(q))
        return q
    elseif type(q) == "table" then
        local limpio = {}
        for k,v in pairs(q) do
            limpio[k] = trim(lsql.escape(q[k]))
        end
        return limpio
    end
    return q
end


--[[Inicio del CRUD Luachi]]--

--Array o tabla datos para ser usado con los metodos crud
local datos = {}
function lsql.datos(array)
    datos = array
end

--agrega un nuevo indice al arreglo datos
function lsql.add_dato(campos)
    for k,v in pairs(campos) do
        datos[k] = campos[k]
    end
    datos = datos
end

--Remueve un campo del array o tabla datos
function lsql.remover(campos)
    for k,v in pairs(campos) do
        datos[v] = nil
    end
    datos = datos
end

--control sanitario para crud 
function lsql.check(condicion,array,tabla,campo,id)

    local update = ' '
    if id then
        update = " and "..campo.."!='"..id.."'";
    end
    if (type(array) == "table") then
        if condicion == 'existe' then
            for campo,valor in pairs(array) do
                local sql = "select "..valor.." from "..tabla.." where "..valor.."='"..datos[valor].."' "..update..""
                if lsql.get_var(sql) then
                    return {campo=valor,valor=datos[valor]} -- valor existe, informo del campo y del valor
                end
            end
            return false --retorno false por que no existe este valor en el campo dado de  la base de datos
        elseif condicion == 'nulo' then
            for campo,valor in pairs(array) do
                if datos[valor] == nil or datos[valor] == '' then
                    return valor -- retorno el campo que es nulo o empty
                end
            end
            return false --retorno false por que nada es nulo o empty
        end
    end

end

--crea una sentencia SQL insert a partir del asociativo arrray o tabla datos
function lsql.crear_insert(tabla)

    local function table_val(array)
        local valores = {}
        for campo, valor in pairs(array) do
            local val = {}
            val = "'"..lsql.escape(valor).."'"
            table.insert(valores,val)
        end
        return table.concat(valores,",")
    end

    local function table_keys(array)
        local campos = {}
        for campo, valor in pairs(array) do
            local key = {}
            key = campo
            table.insert(campos,key)
        end
        return table.concat(campos,",")
    end
    return 'insert into '..tabla..' ('..table_keys(datos)..') '..'values ('..table_val(datos)..')'

end
--crea una sentencia SQL update a partir del asociativo arrray o tabla datos
function lsql.crear_update(tabla,key,id)
    local values = {}
    for campo, valor in pairs(datos) do
        local key = {}
        key = campo.."=".."'"..valor.."'"
        table.insert(values,key)
    end
    return 'update '..tabla..' set '..table.concat(values,",") .. ' where ' .. key .. "='" .. id .."'"
end

--crea una sentencia SQL delete
--@params string
function lsql.crear_delete(tabla,campo,id)
        return "delete from "..tabla.." where "..campo.."='"..id.."'";
end

--[[Fin del CRUD Luachi]]--




