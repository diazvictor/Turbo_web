local table_code = class("table_code")

function RouterHandler:post()
	local arg = {
		nombre = self:get_argument('nombre','',false)
	}
	self:write(table_code:render(arg))
end

local pagina = {
	titulo = 'ResponsiveUI | Table & Code',
	descripcion = 'Table & Code',
	keywords = 'ResponsiveUI, tablas, code, framework, css',
	body = lustache:render(tpl:get("table_code.html"), {
		 header = tpl:get("header.html"),
		footer = tpl:get("footer.html")
	}),
	css = {
		{css = 'responsive-ui.css'},
		{css = 'normalize.css'},
		{css = 'menu.css'},
		{css = 'responsive-ui-utilities.css'},
		{css = 'responsive-ui-icon.css'},
		{css = 'btn-scroll-up.css'},
		{css = 'github.css'},
		{css = 'responsive-ui-dropdown.css'}
	},
	js = {
		{js = 'jquery.js'},
		{js = 'btn-scroll-up.js'},
		{js = 'highlight.pack.js'},
		{js = 'responsive-ui-dropdown.js'},
		{js = 'modulos/scheme.js'}
	}
}

function table_code:render()
	return lustache:render(tpl:get("pagina.html"), pagina)
end

return table_code
