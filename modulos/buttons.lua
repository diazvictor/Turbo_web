local buttons = class("buttons")

function RouterHandler:post()
	local arg = {
		nombre = self:get_argument('nombre','',false)
	}
	self:write(inicio:render(arg))
end

local pagina = {
	titulo = 'ResponsiveUI | Buttons & Icon',
	descripcion = 'Buttons & Icon',
	keywords = 'ResponsiveUI, css, button',
	body = lustache:render(tpl:get("buttons.html"), {
		header = tpl:get("header.html"),
		footer = tpl:get("footer.html")
	}),
	css = {
		{css = 'responsive-ui.css'},
		{css = 'normalize.css'},
		{css = 'menu.css'},
		{css = 'responsive-ui-utilities.css'},
		{css = 'responsive-ui-icon.css'},
		{css = 'btn-scroll-up.css'},
		{css = 'github.css'},
		{css = 'responsive-ui-lightbox.css'},
		{css = 'responsive-ui-tabs.css'},
		{css = 'responsive-ui-accordion.css'},
		{css = 'responsive-ui-dropdown.css'}
	},	
	js = {
		{js = 'jquery.js'},
		{js = 'btn-scroll-up.js'},
		{js = 'highlight.pack.js'},
		{js = 'modulos/buttons.js'},
		{js = 'responsive-ui-dropdown.js'},
		{js = 'modulos/scheme.js'}
	}
}

function buttons:render()
	return lustache:render(tpl:get("pagina.html"), pagina)
end

return buttons
