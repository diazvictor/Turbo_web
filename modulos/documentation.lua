local documentation = class("documentation")

function RouterHandler:post()
	local arg = {
		nombre = self:get_argument('nombre','',false)
	}
	self:write(inicio:render(arg))
end

local pagina = {
	titulo = 'ResponsiveUI | Documentation',
	descripcion = 'Documentation',
	keywords = 'ResponsiveUI, documentation, css, ui, framework',
	body = lustache:render(tpl:get("documentation.html"), {
		header = tpl:get("header.html"),
		footer = tpl:get("footer.html")
	}),
	css = {
		{css = 'responsive-ui.css'},
		{css = 'normalize.css'},
		{css = 'menu.css'},
		{css = 'responsive-ui-utilities.css'},
		{css = 'responsive-ui-icon.css'},
		{css = 'btn-scroll-up.css'},
		{css = 'github.css'},
		{css = 'responsive-ui-lightbox.css'},
		{css = 'responsive-ui-tabs.css'},
		{css = 'responsive-ui-accordion.css'},
		{css = 'responsive-ui-dropdown.css'}
	},
	js = {
		{js = 'jquery.js'},
		{js = 'btn-scroll-up.js'},
		{js = 'highlight.pack.js'},
		{js = 'responsive-ui-lightbox.js'},
		{js = 'responsive-ui-tabs.js'},
		{js = 'responsive-ui-accordion.js'},
		{js = 'responsive-ui-dropdown.js'},
		{js = 'modulos/scheme.js'},
		{js = 'modulos/image.js'}
	}
}

function documentation:render()
	return lustache:render(tpl:get("pagina.html"), pagina)
end

return documentation
