local error = class("error")

function error:render(a)
	local pagina = {
		titulo = 'Error modulo no encontrada',
		body = lustache:render(tpl:get("error.html"),{
			atras = a
		}),
		css = {
			{css = 'responsive-ui.css'},
			{css = 'normalize.css'},
			{css = 'responsive-ui-icon.css'}
		}
	}
	return lustache:render(tpl:get("pagina.html"), pagina)
end

return error
