local forms = class("forms")

function RouterHandler:post()
	local arg = {
		nombre = self:get_argument('nombre','',false)
	}
	self:write(inicio:render(arg))
end

local pagina = {
	titulo = 'ResponsiveUI | Forms',
	descripcion = 'Forms',
	keywords = 'ResponsiveUI, framework, css, formularios',
	body = lustache:render(tpl:get("forms.html"), {
		header = tpl:get("header.html"),
		footer = tpl:get("footer.html")
	}),
	css = {
		{css = 'responsive-ui.css'},
		{css = 'normalize.css'},
		{css = 'menu.css'},
		{css = 'responsive-ui-utilities.css'},
		{css = 'responsive-ui-icon.css'},
		{css = 'btn-scroll-up.css'},
		{css = 'github.css'},
		{css = 'responsive-ui-dropdown.css'},
		{css = 'responsive-ui-validator.css'}
	},
	js = {
		{js  = 'jquery.js'},
		{js  = 'btn-scroll-up.js'},
		{js  = 'highlight.pack.js'},
		{js  = 'responsive-ui-dropdown.js'},
		{js  = 'responsive-ui-validator.js'},
		{js  = 'modulos/scheme.js'},
		{js  = 'modulos/validator.js'}
	}
}

function forms:render()
	return lustache:render(tpl:get("pagina.html"), pagina)
end

return forms
