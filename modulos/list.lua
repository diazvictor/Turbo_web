local list = class("list")

function RouterHandler:post()
	local arg = {
		nombre = self:get_argument('nombre','',false)
	}
	self:write(inicio:render(arg))
end

local pagina = {
	titulo = 'ResponsiveUI | List &amp; Nav',
	descripcion = 'List &amp; Nav',
	keywords = 'ResponsiveUI, framework, css, nav, menu, list, listas',
	body = lustache:render(tpl:get("list.html"), {
		header = tpl:get("header.html"),
		footer = tpl:get("footer.html")
	}),
	css = {
		{css = 'responsive-ui.css'},
		{css = 'normalize.css'},
		{css = 'menu.css'},
		{css = 'responsive-ui-utilities.css'},
		{css = 'responsive-ui-icon.css'},
		{css = 'btn-scroll-up.css'},
		{css = 'github.css'},
		{css = 'responsive-ui-dropdown.css'}
	},
	js = {
		{js = 'jquery.js'},
		{js = 'btn-scroll-up.js'},
		{js = 'highlight.pack.js'},
		{js = 'responsive-ui-dropdown.js'},
		{js = 'modulos/scheme.js'}
	}
}

function list:render()
	return lustache:render(tpl:get("pagina.html"), pagina)
end

return list
