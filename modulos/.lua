local template = class("template")

function RouterHandler:post()
	local arg = {
		nombre = self:get_argument('nombre','',false)
	}
	self:write(inicio:render(arg))
end

local pagina = {
	titulo = 'ResponsiveUI | Template', -- titulo de la pagina
	descripcion = 'template', -- descripcion de la pagina
	keywords = 'ResponsiveUI, framework, css, template, tipografia',
	body = lustache:render(tpl:get("template.html"), {
		header = tpl:get("header.html"), -- header de la pagina
		footer = tpl:get("footer.html") -- footer de la pagina
	}),
	css = {
		{css = 'responsive-ui.css'},
		{css = 'normalize.css'},
		{css = 'menu.css'},
		{css = 'responsive-ui-utilities.css'},
		{css = 'responsive-ui-icon.css'},
		{css = 'btn-scroll-up.css'},
		{css = 'github.css'},
		{css = 'responsive-ui-dropdown.css'}
	},
	js = {
		{js = 'jquery.js'},
		{js = 'btn-scroll-up.js'},
		{js = 'highlight.pack.js'},
		{js = 'responsive-ui-dropdown.js'},
		{js = 'modulos/scheme.js'}
	}
}

function template:render()
	return lustache:render(tpl:get("pagina.html"), pagina)
end

return template
