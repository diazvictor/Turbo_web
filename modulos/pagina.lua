local index = class("index")

function RouterHandler:post()
	local arg = {
		nombre = self:get_argument('nombre','',false)
	}
	self:write(index:render(arg))
end

local pagina = {
	titulo = 'ResponsiveUI | Home',
	descripcion = '',
	body = lustache:render(tpl:get("index.html"), {
		header = tpl:get("header.html"),
		footer = tpl:get("footer.html")
	}),
	css = {
		{css = 'responsive-ui.css'}
		{css = 'normalize.css'},
		{css = 'menu.css'},
		{css = 'responsive-ui-utilities.css'},
		{css = 'responsive-ui-icon.css'},
		{css = 'btn-scroll.css'},
		{css = 'github.css'}
	},
	js = {
		{js = 'jquery.js'},
		{js = 'btn-scroll.js'},
		{js = 'highlight.pack.js'},
	}
}

function index:render()
	return lustache:render(tpl:get("pagina.html"), pagina)
end

return index

