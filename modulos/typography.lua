local typography = class("typography")

function RouterHandler:post()
	local arg = {
		nombre = self:get_argument('nombre','',false)
	}
	self:write(inicio:render(arg))
end

local pagina = {
	titulo = 'ResponsiveUI | Typography',
	descripcion = 'Typography',
	keywords = 'ResponsiveUI, framework, css, Typography, tipografia',
	body = lustache:render(tpl:get("typography.html"), {
		header = tpl:get("header.html"),
		footer = tpl:get("footer.html")
	}),
	css = {
		{css = 'responsive-ui.css'},
		{css = 'normalize.css'},
		{css = 'menu.css'},
		{css = 'responsive-ui-utilities.css'},
		{css = 'responsive-ui-icon.css'},
		{css = 'btn-scroll-up.css'},
		{css = 'github.css'},
		{css = 'responsive-ui-dropdown.css'}
	},
	js = {
		{js = 'jquery.js'},
		{js = 'btn-scroll-up.js'},
		{js = 'highlight.pack.js'},
		{js = 'responsive-ui-dropdown.js'},
		{js = 'modulos/scheme.js'}
	}
}

function typography:render()
	return lustache:render(tpl:get("pagina.html"), pagina)
end

return typography
