/****************************************************
              ____                  _     _
             / ___|_   _  __ _  ___| |__ (_)
            | |  _| | | |/ _` |/ __| '_ \| |
            | |_| | |_| | (_| | (__| | | | |
             \____|\__,_|\__,_|\___|_| |_|_|

  Copyright (c) 2015 Diaz Victor aka (Master Vitronic)
    <vitronic2@gmail.com> <vitronic@vitronic.com.ve>

*****************************************************/


/**
 * Very simple Validator Plugin from Guachi Framework
 *
 * funciones addClass y removeClass tomadas de :
 * http://www.kirupa.com/html5/setting_css_styles_using_javascript.htm
 *
 *
 * @autor Diaz Victor aka (Master Vitronic)
 * @email <vitronic2@gmail.com>
 * @email <vitronic@vitronic.com.ve>
 * @package guachi validator Form
 * @depend only JavaScript
 *
 * Copyright (c) 2015 Diaz Victor aka (Master Vitronic)
 * Released under the LGPLv3
 * https://www.gnu.org/licenses/lgpl.html
 */

var Guachi_Def = {
    class_input: 'validate_input',
    class_alert: 'validate_alert',
    id_cont_msg: 'cont_msg',
    id_destino:  'mensaje'
};

Guachi = {
    addClass: function (element, classToAdd) {
        var currentClassValue = element.className;
        if (currentClassValue.indexOf(classToAdd) == -1) {
            if ((currentClassValue == null) || (currentClassValue === "")) {
                element.className = classToAdd;
            } else {
                element.className += " " + classToAdd;
            }
        } 
    },
    removeClass: function (element, classToRemove) {
        var currentClassValue = element.className;
        if (currentClassValue == classToRemove) {
            element.className = "";
            return;
        }
        var classValues = currentClassValue.split(" ");
        var filteredList = [];
        for (var i = 0; i < classValues.length; i++) {
            if (classToRemove != classValues[i]) {
                filteredList.push(classValues[i]);
            }
        }
        element.className = filteredList.join(" ");
    },
    clear_class: function () {
        var rem = document.querySelectorAll('.' + Guachi_Def.class_input);
        for (var i = 0; i < rem.length; i++) {
            Guachi.removeClass(rem[i], Guachi_Def.class_alert);
        }
    },
    show: function (id) {
        document.getElementById(id).style.display = 'block';
    },
    hide: function (id) {
        document.getElementById(id).style.display = 'none';
    },
    focus: function (id) {
        document.getElementById(id).focus();
    },
    html: function (id, message) {
        document.getElementById(id).innerHTML = message;
    },
    accion: function (id, mensaje) {
        Guachi.clear_class();
        Guachi.focus(id);
        Guachi.addClass(document.getElementById(id), Guachi_Def.class_alert);
        Guachi.show(Guachi_Def.id_cont_msg);
        Guachi.html(Guachi_Def.id_destino, mensaje);
    },
    integer: function (id) {
        return /^-?[0-9]+$/.test(document.getElementById(id).value);
    },
    float: function (id) {
        return /^([0-9])*[.]?[0-9]*$/.test(document.getElementById(id).value); //return !isNaN(document.getElementById(id).value);
    },
    macaddr: function (id) {
        return /^([a-fA-F0-9]{2}:){5}[a-fA-F0-9]{2}$/.test(document.getElementById(id).value);
    },
    ip4addres: function (id) {
        return /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(document.getElementById(id).value);
    },
    empty: function (id) {
        input = document.getElementById(id).value;
        return input == null || input.length == 0 || /^\s+$/.test(input);
    },
    email: function (id) {
        return /(\w+)(\.?)(\w*)(\@{1})(\w+)(\.?)(\w*)(\.{1})(\w{2,3})/.test(document.getElementById(id).value);
    },
    rif: function (id) {
        return /^([JGVEP]{1})-([0-9]{8})-([0-9]{1})$/.test(document.getElementById(id).value);
    },
    c_i: function (id) {
        return /^([0-9]{8})$/.test(document.getElementById(id).value);
    },
    regexp: function (id,regexp) {
        return regexp.test(document.getElementById(id).value);
    },
    validate: function (campos) {
        var result = true;
        for (var id in campos) {
            var mensaje = campos[id].mensaje;
            var tipo = campos[id].tipo;
            var required = campos[id].required ? true : false;
            switch (tipo) {
                case 'text':
                    function empty() {
                    if (Guachi.empty(id)) {
                        Guachi.accion(id, mensaje);
                        result = false;
                    }
                    }
                    if (required === true) {empty();break;}
                    empty();
                    break;
                case 'integer':
                    function integer() {
                        if (Guachi.integer(id) == false) {
                            Guachi.accion(id, mensaje);
                            result = false;
                        }
                    }
                    if (required === true) {integer();break;}
                    if (!Guachi.empty(id)) {integer();}
                    break;
                case 'float':
                    function float() {
                        if (Guachi.float(id) == false || Guachi.empty(id)) {
                            Guachi.accion(id, mensaje);
                            result = false;
                        }
                    }
                    if (required === true) {float();break;}
                    if (!Guachi.empty(id)) {float();}
                    break;
                case 'macaddr':
                    function macaddr() {
                        if (Guachi.macaddr(id) == false) {
                            Guachi.accion(id, mensaje);
                            result = false;
                        }
                    }
                    if (required === true) {macaddr();break;}
                    if (!Guachi.empty(id)) {macaddr();}
                    break;
                case 'ip4addres':
                    function ip4addres() {
                        if (Guachi.ip4addres(id) == false) {
                            Guachi.accion(id, mensaje);
                            result = false;
                        }
                    }
                    if (required === true) {ip4addres();break;}
                    if (!Guachi.empty(id)) {ip4addres();}
                    break;
                case 'email':
                    function email() {
                        if (Guachi.email(id) == false) {
                            Guachi.accion(id, mensaje);
                            result = false;
                        }
                    }
                    if (required === true) {email();break;}
                    if (!Guachi.empty(id)) {email();}
                    break;
                case 'rif':
                    function rif() {
                        if (Guachi.rif(id) == false) {
                            Guachi.accion(id, mensaje);
                            result = false;
                        }
                    }
                    if (required === true) {rif();break;}
                    if (!Guachi.empty(id)) {rif();}
                    break;
                case 'c_i':
                    function c_i() {
                        if (Guachi.c_i(id) == false) {
                            Guachi.accion(id, mensaje);
                            result = false;
                        }
                    }
                    if (required === true) {c_i();break;}
                    if (!Guachi.empty(id)) {c_i();}
                    break;
                case 'custom':
                    function RegExp() {
                        var RegExp = campos[id].RegExp;
                        if (Guachi.regexp(id,RegExp) == false) {
                            Guachi.accion(id, mensaje);
                            result = false;
                        }
                    }
                    if (required === true) {RegExp();break;}
                    if (!Guachi.empty(id)) {RegExp();}
                    break;
                default:
                    Guachi.accion(id, 'Tipo no reconocido');
                    result = false;
                    break;
            }
        }
        return result ? true : false;
    }
};

