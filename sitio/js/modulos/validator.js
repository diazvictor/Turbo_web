/*
 * validator.js
 * 
 * Copyright 2016 Victor Diaz <victor.vector@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

var campos = {
	emailvalidate: {
		required: true,
		tipo: 'email',
		mensaje: 'Email es un campo obligatorio'
	},
	passwordvalidate: {
		required: true,
		tipo: 'text',
		mensaje: 'Password es un campo obligatorio'
	},
	textvalidate: {
		required: true,
		tipo: 'text',
		mensaje: 'Text es un campo obligatorio'
	}
};
document.getElementById('buttonvalidate').addEventListener('click', function () {
	if (Guachi.validate(campos)) {
		Guachi.hide(Guachi_Def.id_cont_msg);
		alert("Esto es Guachi_Validator");
	}
}, false);
