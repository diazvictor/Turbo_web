/*!
* @package   ResponsiveUI
* @filename  btn-scroll-up.js
* @version   1.0
* @autor     Diaz Urbaneja Victor Eduardo Diex <victor.vector008@gmail.com>
* @date      15.12.2017 10:34:27 -04
*/

$(document).ready(function(){
	$('.btn-scroll-up').click(function(){
		$('body, html').animate({
			scrollTop: '0px'
		});
	});

	$(window).scroll(function(){
		if( $(this).scrollTop() > 800 ){
			// $('.btn-scroll-up').slideDown(300);
			$('.btn-scroll-up').fadeIn();
			// $('.btn-scroll-up').hiden()
		} else {
			// $('.btn-scroll-up').slideUp(300);
			$('.btn-scroll-up').fadeOut();
			// $('.btn-scroll-up').show()
		}
	});
});
