/*!
* @package   ResponsiveUI
* @filename  responsive-ui-tabs.js
* @version   1.0
* @autor     Diaz Urbaneja Victor Eduardo Diex <victor.vector008@gmail.com>
* @date      2016
*/

$(document).ready(function(){
	$('ul.tabs li').click(function(){
		var tab_id = $(this).attr('data-tab');

		$('ul.tabs li').removeClass('current');
		$('.tab-content').removeClass('current');

		$(this).addClass('current');
		$("#"+tab_id).addClass('current');
	})
})
