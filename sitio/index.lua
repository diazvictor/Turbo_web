#!/usr/bin/env luajit

package.path = package.path .. ";../?.lua"
TURBO_SSL    = true
turbo        = require("turbo")
util         = require("turbo.util")
conf         = require("conf")
lustache     = require("lustache")
tpl          = require("lib.tpl")

RouterHandler = class("RouterHandler", turbo.web.RequestHandler)
function RouterHandler:get(modulo)
    if util.file_exists(conf.path.."/modulos/"..modulo..".lua") == true then
        local view = require("modulos."..modulo)
		local modulo_anterior = self:get_cookie("modulo_actual")
		self:set_cookie("modulo_anterior", modulo_anterior)
		self:set_cookie("modulo_actual", modulo)
        self:write(view:render())
        return
    else
        local view = require("modulos.error")
		self:set_status(404)
		self:write(view:render(self:get_cookie("modulo_actual")))
        return
    end
end

local app = turbo.web.Application:new({
	{"^/$", turbo.web.RedirectHandler,"/index"},
	{"^/css/(.*)$", turbo.web.StaticFileHandler, "./css/"},
	{"^/js/(.*)$", turbo.web.StaticFileHandler, "./js/"},
	{"^/fonts/(.*)$", turbo.web.StaticFileHandler, "./fonts/"},
	{"^/img/(.*)$", turbo.web.StaticFileHandler, "./img/"},
	{"^/static/(.*)$", turbo.web.StaticFileHandler, "./static/"},
	{"^/(.*)$", RouterHandler}
}

):listen(conf.port)
turbo.log.success("Binding to port: " .. tostring(conf.port))
turbo.ioloop.instance():start()
