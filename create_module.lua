#!/usr/bin/lua5.3

--[[!
 @package   ResponsiveUI
 @filename  create_module.lua
 @version   1.0
 @autor     Diaz Urbaneja Victor Eduardo Diex <victor.vector008@gmail.com>
 @date      07.01.2018 10:04:52
]]--

package.path = package.path .. ";../?.lua"
require("conf")
require("lib.functions")

local templates = conf.path..'/extras/templates/'
print("nombre del modulo")
local modulo = io.read()
-- local modulo = arg[1]

if modulo == nil then
    print('Falta nombre de modulo')
    return
end

if file_check(conf.path.."/modulos/"..modulo..".lua") == true then
    print('El modulo ya existe')
    return
end

os.execute('cp '.. templates..'template.lua ' .. conf.path ..'/modulos/' ..  modulo .. '.lua')
os.execute('cp '.. templates..'template.html ' .. conf.path ..'/vistas/'.. conf.tema ..'/'.. modulo .. '.html')
-- os.execute('cp '.. templates..'css.css ' .. ROOT ..'/sitio/css/modulos/'.. modulo .. '.css')
-- os.execute('cp '.. templates..'js.js ' .. ROOT ..'/sitio/js/modulos/'.. modulo .. '.js')
