--[[!
 @package   ResponsiveUI
 @filename  conf.lua
 @version   1.3
 @autor     Diaz Urbaneja Victor Eduardo Diex <victor.vector008@gmail.com>
 @date      2017
]]--

conf = {} -- configuracion

conf.uri = "http://localhost" -- url de la aplicacion

conf.path = '/var/www/dnsweb' -- ruta base del projecto

conf.tema = 'responsive-ui' -- nombre del tema a usar

conf.port = 8888 -- turbo listening port

conf.db = {} -- confuracion de la base de datos

conf.db.init = false -- inicializar base de datos (crear tablas)

conf.db.path = "db/aports.db" -- path to the sqlite db

conf.db.fields = {"provides", "depends", "install_if"} -- multi value db fields

conf.logging = true -- debug logging. true to enable to stdout, syslog to syslog
--[[
	set the branches,repos,archs you want to include
--]]
conf.branches = {"latest-stable", "edge"}
conf.repos = {"main", "community", "testing"}
conf.archs = {"x86", "x86_64", "armhf"}

conf.mirror = "/home/victor/.packages" -- location of the mirror on disk

conf.tpl = "vistas" -- directory where views are stored

return conf
